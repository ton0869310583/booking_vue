const config = {
    URL: 'https://apiappflutter.herokuapp.com',
    path: {
        login: "/api/users/login",
        user: '/api/users/getusers',
        getq: '/api/dates/getdatesq',
        date: '/api/dates/getdates',
        dateq: '/api/dates/getdatesq',
        insert: '/api/dates/insertqueue',
        remove: '/api/dates/remove'
    }
}

module.exports.config = config