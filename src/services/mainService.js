// ---------- encoding & decoding ----------
const Base64 = {
    encoding: function (data) {
        return btoa(unescape(encodeURIComponent(Buffer.from(String(data), "utf8").toString("base64"))));
    },
    decoding: function (data) {
        return decodeURIComponent(escape(atob(Buffer.from(String(data), "base64").toString("utf8"))));
    }
}
module.exports.Base64 = Base64;


// ---------- Cookie ----------
module.exports.saveCookie = function saveCookie(name, value, days) {
    var date = new Date();
    if (days)
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000)); //n days
    else
        date.setTime(date.getTime() + (1 * 60 * 60 * 1000)); //106 hour 

    var expires = "; expires=" + date.toUTCString();
    var endcode = Base64.encoding(value)
    document.cookie = name + "=" + endcode + expires + "; path=/";
}


module.exports.readCookie = function readCookie(name) {
    try {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];

            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return Base64.decoding(c.substring(nameEQ.length, c.length));
        }
        return null;
    } catch (error) {
        return null
    }
}

module.exports.removeCookie = function removeCookie(name) {
    var date = new Date();
    date.setTime(date.getTime() + (-1 * 24 * 60 * 60 * 1000)); //n days
    var expires = "; expires=" + date.toUTCString();
    document.cookie = name + "=" + '' + expires + "; path=/";
}


// ---------- localStorage ----------

module.exports.saveLocal = function saveLocal(name, value) {
    var endcode = Base64.encoding(value)
    localStorage.setItem(name, endcode);
}

module.exports.readLocal = function saveLocal(name) {
    return Base64.decoding(localStorage.getItem(name))
}

module.exports.removeLocal = function saveLocal(name) {
    localStorage.removeItem(name);
}