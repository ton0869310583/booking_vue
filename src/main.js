import { createApp } from "vue";
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import App from "./App.vue";
import router from "./router";
import store from "./store";


import VueBarcode from '@chenfengyuan/vue-barcode';

const app = createApp(App)
app.use(store)
app.use(router)
app.component(VueBarcode.name, VueBarcode);
app.mount('#app')

// createApp(App).use(store).use(router).mount("#app");


// import VueBarcode from '@chenfengyuan/vue-barcode';

// const app = createApp(App);


// app.use(store)
// app.use(router)
// app.use(VueSweetalert2)
// app.component(VueBarcode.name, VueBarcode);
// app.mount('#app')