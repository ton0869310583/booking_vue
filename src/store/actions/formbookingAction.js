import service from '../../services/mainService'
import config from '../../services/config'
import axios from 'axios'


export const formbookingAction = {

    state: {
        dataQ: null,
        loadremove: false,
        complete: null

    },
    mutations: {
        MUT_DATEQ(state, value) {
            state.dataQ = value
        },
        MUT_LR(state, value) {
            state.loadremove = value
        },
        MUT_COM(state, value) {
            state.complete = value
        }

    },
    getters: {


        GET_DATEQ(state) {
            return state.dataQ
        },

        GET_LR(state) {
            return state.loadremove
        },

        GET_COM(state) {
            return state.complete
        }
    },
    actions: {

        async getDatrQAction(context, value) {
            context.commit('MUT_DATEQ', null)
            var url = `${config.config.URL}${config.config.path.date}`
            var token = service.readCookie('token')
            try {



                var configax = {
                    method: 'post',
                    url: url,
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                }
                var res = await axios(configax)
                if (res.status === 200) {

                    await service.saveLocal('dateq', JSON.stringify(res.data.data))
                    var getq = await JSON.parse(service.readLocal('dateq'))
                    context.commit('MUT_DATEQ', getq)
                    // console.log(getq);

                }





            } catch (error) {
                console.log(error);
                context.commit('MUT_DATEQ', null)
            }

        },
        async getdateq(context, value) {
            var dateq = JSON.parse(service.readLocal('dateq'))
            context.commit('MUT_DATEQ', dateq)


        },
        async removeAction(context, value) {
            // console.log(value);
            context.commit('MUT_LR', true)
            var token = await service.readCookie('token')
            var url = `${config.config.URL}${config.config.path.remove}`
            try {
                console.log(value.date);
                console.log(value.time);
                var data = JSON.stringify({
                    "date": value.date,
                    "time": value.time
                });
                var configax = {
                    method: 'post',
                    url: url,
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    data: data
                }
                var res = await axios(configax)

                if (res.status === 200) {
                  //  console.log('204');
                    await service.removeLocal('dateq')
                    //console.log('removeLocal');
                    context.commit('MUT_COM', true)
                    //console.log('MUT_COM');
                    context.commit('MUT_DATEQ', null)
                   // console.log('MUT_DATEQ');
                }else{
                    await service.removeLocal('dateq')
                     context.commit('MUT_COM', false)
                     context.commit('MUT_DATEQ', null)
                }


            } catch (error) {
               
            }
        },
        async insertAction(context, value) {
            // console.log(value);
            context.commit('MUT_LR', true)
            var token = await service.readCookie('token')
            var url = `${config.config.URL}${config.config.path.insert}`
            try {
                console.log(value.date);
                console.log(value.time);
                var data = JSON.stringify({
                    "date": value.date,
                    "time": value.time
                });
                var configax = {
                    method: 'post',
                    url: url,
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json'
                    },
                    data: data
                }
                var res = await axios(configax)

                if (res.status === 201) {
                  //  console.log('204');
                    await service.removeLocal('dateq')
                    //console.log('removeLocal');
                    context.commit('MUT_COM', true)
                    //console.log('MUT_COM');
                    context.commit('MUT_DATEQ', null)
                   // console.log('MUT_DATEQ');
                }else{
                    await service.removeLocal('dateq')
                     context.commit('MUT_COM', false)
                     context.commit('MUT_DATEQ', null)
                }


            } catch (error) {
               
            }
        }
        ,
        async initAction(context, value) {
            context.commit('MUT_COM', null)

            context.commit('MUT_LR', false)


        },
    },




}

