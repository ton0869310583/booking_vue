import service from '../../services/mainService'
import config from '../../services/config'
import axios from 'axios'
import store from '../index'


export const loginAction = {
    state: {
        load: false,
        login: null,
        auth: false,
        user: null

    },
    mutations: {
        MUT_LOAD(state, value) {
            state.load = value
        }
        ,
        MUT_LOGIN(state, value) {
            state.login = value
        },
        MUT_AUTH(state, value) {
            state.auth = value
        },
        MUT_USER(state, value) {
            state.user = value
        }
    },
    getters: {


        GET_LOAD(state) {
            return state.load
        },
        GET_LOGIN(state) {
            return state.login
        }
        ,
        GET_AUTH(state) {
            return state.auth
        },
        GET_USER(state) {
            return state.user
        }
    },
    actions: {
        async loginAction(context, value) {

            context.commit('MUT_LOAD', true)
            var url = `${config.config.URL}${config.config.path.login}`
            try {
                var res = await axios.post(url, { 'phone': value }, {
                })

                if (res.status === 200) {
                    //console.log(res.data.token);
                    service.saveCookie('token', res.data.token)
                    var url1 = `${config.config.URL}${config.config.path.user}`
                    var configax = {
                        method: 'post',
                        url: url1,
                        headers: {
                            'Authorization': 'Bearer ' + res.data.token
                        }
                    }
                    var res1 = await axios(configax)
                    if (res1.status === 200) {
                        var data = {
                            firstname: res1.data.firstname,
                            lastname: res1.data.lastname,
                            emp_id: res1.data.emp_id
                        }

                        service.saveCookie('user', JSON.stringify(data))

                        context.commit('MUT_LOGIN', true)
                    }

                    context.commit('MUT_LOAD', false)

                    setTimeout(async () => {
                        context.commit('MUT_LOGIN', null)
                        context.commit('MUT_AUTH', true)
                        service.saveCookie('auth', true)


                    }, 3000);

                }
                else {
                    context.commit('MUT_LOGIN', false)
                    setTimeout(() => {
                        context.commit('MUT_LOGIN', null)
                    }, 3000);
                }
            } catch (error) {
                context.commit('MUT_LOAD', false)
                context.commit('MUT_LOGIN', false)
                setTimeout(() => {
                    context.commit('MUT_LOGIN', null)
                }, 3000);
            }

        },

        async getAuthAction(context, value) {
            let auth = service.readCookie('auth')
            let user = service.readCookie('user')
            let tk = service.readCookie('token')
            if (auth ||(user && tk)) {
                context.commit('MUT_AUTH', true)
            }
            else {
                context.commit('MUT_AUTH', false)
            }

        },

        async logOutAction(context, value) {

            service.removeCookie('auth')
            service.removeCookie('user')
            service.removeCookie('token')
            service.removeCookie('queue')
            service.removeLocal('dateq')

            context.commit('MUT_DATEQ', null)
            context.commit('MUT_AUTH', false)
            context.commit('MUT_LOGIN', null)
            context.commit('MUT_LOAD', false)
            context.commit('MUT_USER', null)
            context.commit('MUT_QUEUE', null)
            // load: false,
            // login: null,
            // auth: false

        },

        async userAction(context, value) {
            var uesr = JSON.parse(service.readCookie('user'))
            context.commit('MUT_USER', uesr)

        }

    },
}

