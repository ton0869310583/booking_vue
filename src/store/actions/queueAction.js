import service from '../../services/mainService'
import config from '../../services/config'
import axios from 'axios'


export const queueAction = {

    state: {
        gueue: null,


    },
    mutations: {
        MUT_QUEUE(state, value) {
            state.gueue = value
        }

    },
    getters: {


        GET_QUEUE(state) {
            return state.gueue
        }
    },
    actions: {
        async getGueueAction(context, value) {
            var url = `${config.config.URL}${config.config.path.getq}`
            var token = service.readCookie('token')
            try {



                var configax = {
                    method: 'post',
                    url: url,
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                }
                var res = await axios(configax)
                if (res.status === 200) {
                   // console.log(res.data.data);
                    service.saveCookie('queue', JSON.stringify(res.data.data))
                        var getq = JSON.parse(service.readCookie('queue'))
                 
                   // console.log(getq);
                    context.commit('MUT_QUEUE', getq)
                }





            } catch (error) {
                console.log(error);
                context.commit('MUT_QUEUE', null)
            }

        }
    }



}

