
import { createStore } from "vuex";
import { loginAction } from './actions/loginAction'
import { queueAction } from './actions/queueAction'
import {formbookingAction} from'./actions/formbookingAction'
import {testVhtmlAction} from './actions/testVhtmlAction'
export default createStore({

  modules: {
    loginAction: loginAction,
    queueAction: queueAction,
    formbookingAction:formbookingAction,
    testVhtmlAction:testVhtmlAction
  },
});
