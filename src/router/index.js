import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Login from '../views/Login.vue'
import Booking from '../views/FormBooking.vue'
import Boo from '../views/Boo.vue'
import service from '../services/mainService'
import store from '../store/index'

const routes = [
  {
    path: "/",
    name: "Home",
    meta: { requiresAuth: true },
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/booking",
    name: "Booking",
    meta: { requiresAuth: true },
    component: Booking,
    // component: () =>
    //   import(/* webpackChunkName: "about" */ "../views/FormBooking.vue"),
  },
  {
    path: "/bootoo",
    name: "Boo",
    component: Boo,
  }


];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let getUser = service.readCookie('user')
    let gettoken = service.readCookie('token')
    store.dispatch('getAuthAction')
    if (!getUser || !gettoken) {
      next({
        path: '/login'
      })
      return
    }
  }


  next() // make sure to always call next()!

})

export default router;
